#!/usr/bin/env python
# coding: utf-8

# 
# 
# ![Image](https://th.bing.com/th/id/AMMS_4b0896e1c3572451d14093065f0c1e61?w=260&h=174&c=7&o=5&pid=1.7)
# ![Image](https://th.bing.com/th/id/OIP.koULmIorq1ZJrJcwQe8kSQAAAA?pid=Api&w=229&h=137&c=7)
# ![Image](https://th.bing.com/th/id/OIP.7SIYXAaSosB4RocW5Vog5QHaHC?w=148&h=146&c=7&o=5&pid=1.7)
# 
# # Démonstration d'un article intéractif et reproductible

# - Interroger le lien entre la campagne d'évaluation HCERES et l'évolution des dépôts HAL

# ## Par Michael Nauge
# 

# # RESUME 

# ### Démonstration d'un article interactif dans un article interactif. Bien que principalement démonstratif, nous allons tout de même amorcer une étude de données réelles en lien avec la science ouverte.

# ### Un article interactif contient 3 types de cellules :

# - Cellule : texte avec balisage léger Markdown
# - Cellule : code exécutable
# - Cellule : résultat de code (affichage de chiffre, tableau, graphique)

# ### Une cellule a 3 états :

# - État : exécuté/affiché
# - État : en cours d'exécution (uniquement pour les cellules de code)
# - État : édition

# ### L'objectif de cet article intéractif est de montrer l'intérêt de ces cellules par un exemple d'exploration de données.

# ## Protocole

# Nous souhaitons nous interroger sur le lien pouvant exister entre le nombre de notices déposées sur HAL et les dates d'évaluations HCERES.
# Pour celà nous souhaitons obtenir le nombre de notices déposées par an pour un échantillon de laboratoire tiré aléatoirement mais dont les dates 
# d'évaluation HCERES sont identiques. Dans cette étude nous nous concentrons autours de la campagne d'évaluation 2016-2017 afin de disposer 
# d'observation antérieurs et postérieurs pour évaluer les phénomènes d'accélarations et décélération des dépôts.. 
# L'extraction de ces informations peut être réalisé avec le logiciel extract HAL ou directement en réalisant un script ad-hoc réalisant des requêtes s
# ur l'API HAL.Dans cet article nous avons choisi la seconde méthode pour maîtriser tous les parmètres et montrer l'intéret d'un article interactif ;-).
# Le code d'extraction de données est disponible en Annexe de ce document. Il ne s'agit pas seulement du code source, il est bien directement exécutable dans 
# l'article !

# ## Construction du dataset

#  * La cellule ci-dessous est une cellule de code. Pour l'exécuter, il faut la selectionner puis taper sur la combinaison de touche : MAJ+ENTER.

# In[9]:


# Encode/Decode Python objects as JSON strings and vice-a-versa
# In Python, the json module provides an API similar to convert in-memory Python objects to a serialized representation 
# known as JavaScript Object Notation (JSON) and vice-a-versa.


import requests
import json
import pandas as pd

def listCollectionToCSV(listCollection, listYears, pathCSV):
    # requets nb depots par an pour les collections choisies
    # années d'interet : 2003 à 2021
    
    
    # liste des colonnes du tableur résultat
    df = pd.DataFrame(columns = ["collectionName","year","nbByProducedDate","nbBySubmittedDate","nbByReleasedDate"])
    
    for collectionName in listCollection:
        for year in listYears:
            
            #apiUrl = "https://api.archives-ouvertes.fr/search/XLIM/?q=producedDateY_i:[2008 TO 2009]"
            
            apiUrl = "https://api.archives-ouvertes.fr/search/"+collectionName+"/?q=producedDateY_i:["+str(year)+" TO "+str(year+1)+"]"
            response = requests.get(apiUrl)
            jr = json.loads(response.text)
            
            nbResProduced = jr['response']['numFound']
            
            apiUrl = "https://api.archives-ouvertes.fr/search/"+collectionName+"/?q=submittedDateY_i:["+str(year)+" TO "+str(year+1)+"]"
            response = requests.get(apiUrl)
            jr = json.loads(response.text)
            nbResSubmitted = jr['response']['numFound']

            apiUrl = "https://api.archives-ouvertes.fr/search/"+collectionName+"/?q=releasedDateY_i:["+str(year)+" TO "+str(year+1)+"]"
            response = requests.get(apiUrl)
            jr = json.loads(response.text)
            nbResReleased = jr['response']['numFound']

            #apiUrl = "https://api.archives-ouvertes.fr/search/XLIM/?q=publicationDateY_i:[2008 TO 2009]"
            apiUrl = "https://api.archives-ouvertes.fr/search/"+collectionName+"/?q=publicationDateY_i:["+str(year)+" TO "+str(year+1)+"]"
            response = requests.get(apiUrl)
            jr = json.loads(response.text)
            nbResPublication = jr['response']['numFound']
             
            dicRow = {'collectionName':collectionName,"year":year,"nbByProducedDate":nbResProduced,"nbBySubmittedDate":nbResSubmitted, "nbByReleasedDate":nbResReleased, "nbByPublicationDate":nbResPublication}
            
            print(dicRow)
            
            df = df.append(dicRow, ignore_index=True)

    return df


# ### Regénérer les données¶
# Les données présentées dans cet articles sont produitent par le code ci-dessous :

# In[ ]:


# Un exemple d'extraction d'une seule collection. Ici l'établissement Université de Poitiers
#listLabs = ["UNIV-POITIERS"]
#listYears = range(2003,2021)
#fileDataUP = "https://api.archives-ouvertes.fr/docs/search/?schema=field-types#field-types"


# In[10]:


# Extraction de plusieurs collection

listLabs = ["XLIM","PPRIME","FORELL","CERCA", "IC2MP"]
listYears = range(2003,2021)
fileData = "https://api.archives-ouvertes.fr/docs/search/?schema=field-types#field-types"

dfHal = listCollectionToCSV(listLabs, listYears, fileData)


# ### Enregister et exporter les données dans un tableur au format csv

# In[ ]:


#dfHal.to_csv('dfHal.csv',index=False)


# ## Créer un echantillon aléatoire

# ### Si l'on exécute plusieurs fois la cellule ci-dessous, nous visualisons un nouvel échantillons aléatoire.

# In[11]:


dfHal.sample(n=5)


# ### Description du set de données
# 
# - collectionName : variable qualitative nominale (STRING) : le nom d'une collection, ici des laboratoiresyear 
# - year : variable temporelle discrète (INTEGER) : une année d'intérêt
# - nbByProducedDate : variable quantitative discrète (INTEGER) : le nombre de notice dont l'année de la production du contenu est l'année d'intérêt.
# - nbBySubmittedDate : variable quantitative discrète (INTEGER) : le nombre de notice dont l'année de soumission de la notice sous HAL est l'année d'intérêt.
# - nbByReleasedDate : variable quantitative discrète (INTEGER) : le nombre de notice dont l'année où la notice est visible sous HAL est l'année d'intérêt.
# - nbByPublicationDate : variable quantitative discrète (INTEGER) : le nombre de notice dont l'année de la publication (éditeur) du contenu est l'année d'intérêt.
# 
# https://api.archives-ouvertes.fr/docs/search/?schema=field-types#field-types
# 
# Nous allons dans cette article particulièrement nous intéresser à la SubmittedDate qui est l'année de la création de la notice sous HAL indépendament du moment où la notice est modédée. 
# 
# ### Informations sur notre tableau de données
# 
# NB : Il est conseillé d'exécuter toutes les cellules de code précédentes à une cellule de code car il y a souvent une dépendance entre plusieurs cellules.
# Par exemple le chargement/import d'une librairie une fois dans une cellule alors que de nombreuses cellules suivantes en dépendendent ou l'instanciation d'une variable préalable.

# In[12]:


# combien de lignes ?
print('nb lines : ', len(dfHal))


# In[31]:


# combien de lignes par labo. ?
print('nb lines by lab. :')
dfHal['collectionName'].value_counts()


# In[13]:


# vérifier les types des colonnes
dfHal.dtypes


# In[14]:


# ré-affecter un type à une colonne
dfHal.astype({'nbByPublicationDate': 'int64'}).dtypes


# In[26]:


# transformer certaines colonnes en faveur numeriques
dfHal[["year", "nbByProducedDate", "nbBySubmittedDate", "nbByReleasedDate", "nbByPublicationDate" ]]= dfHal[["year", "nbByProducedDate", "nbBySubmittedDate", "nbByReleasedDate", "nbByPublicationDate" ]].apply(pd.to_numeric)


# In[30]:


dfHal.dtypes


# ### Faire un calcul sur les données
# 

# In[15]:


# calcul de moyenne par colonne
dfHal.mean()


# In[33]:


# calcul de median par colonne
dfHal.median()


# In[32]:


# calcul de median sur un échantillon de 30% du tableau de données
dfHal.sample(frac=0.3).median()


# *NB : Nous remarquons ici une différence importante entre la moyenne et la médiane !*

# In[34]:


#calcul de moyenne de dépôts par an (tous laboratoires confondus)
dfHal.groupby("year").mean()


# In[18]:


#description des données en une fois

print(dfHal.describe(include='all'))


# ### Afficher un graphique

# In[35]:


get_ipython().run_line_magic('matplotlib', 'inline')

# Matplotlib Scatter Plot
import matplotlib.pyplot as plt

plt.figure()
plt.scatter('year', 'nbBySubmittedDate',data=dfHal)
plt.xlabel('year')
plt.ylabel('nbBySubmittedDate')
plt.show()


# In[20]:


import seaborn as sns

sns.set_style("whitegrid")


# In[ ]:


#!pip install nodejs


# In[ ]:


#!pip install ipympl


# In[ ]:


#!pip install ipywidgets


# In[39]:


get_ipython().run_line_magic('matplotlib', 'ipympl')
plt.figure("Submitted over years") 
# graphique sous forme de ligne
sns.lineplot(data=dfHal, x="year", y="nbBySubmittedDate", hue="collectionName", style="collectionName")

#graphique sous forme de point
#sns.scatterplot(data=dfHal, x="year", y="nbBySubmittedDate", hue="collectionName", style="collectionName" )


# *Les reviewers demandent le même graphique en N&B, plus grand*

# In[40]:


get_ipython().run_line_magic('matplotlib', 'ipympl')
fig, ax = plt.subplots()

# the size of A4 paper
fig.set_size_inches(11.7, 8.27)
sns.set_style("whitegrid")
ax.set(xticks=range(2003,2021))

ax.set_title("submitted over years (greyscale)")

fig.set_size_inches(11.7, 8.27)
sns_plot = sns.lineplot(data=dfHal, x="year", y="nbBySubmittedDate", hue="collectionName", style="collectionName", palette="Greys" )


#  ### Sauvegarder/exporter la figure

# In[45]:


sns_plot.figure.savefig(r"C:\Users\germa\Desktop\JupyterLab\sub-years_greyscale.png",dpi=200)


# ### Compléter les observations proposées

# > Que diriez vous d'ajouter la visualisation de la médiane et des quartiles ?

# In[46]:


# Ajouter la visualisation en box plot

get_ipython().run_line_magic('matplotlib', 'ipympl')
plt.figure("box plot view")
sns.boxplot(data=dfHal, x = "year", y="nbBySubmittedDate")


# ### On rejette un laboratoire car il a un comportement hors norme

# In[47]:


dfHalWihoutXlim = dfHal[dfHal["collectionName"] != "XLIM"]
dfHalWihoutXlim.sample(10)


# ### Vérification

# In[48]:


# générer le nouveau graphique
get_ipython().run_line_magic('matplotlib', 'ipympl')
plt.figure()
sns.boxplot(data=dfHalWihoutXlim, x = "year", y="nbBySubmittedDate")
plt.figure()
sns.boxplot(data=dfHal, x = "year", y="nbBySubmittedDate")


# ## Conclusion
# Nous remarquons une forte inégalité des dépôts en fonction des laboratoires. Il n'y a pas de conclusion évidente à tirer. Les raisons imaginées sont disciplinaires, le rattachement au CNRS et la présence de personnels dédiés/ressources ayant un impact sur la prise de conscience de l'interêt de déposer sous HAL. L'analyse au niveau établissement de la courbe (bleue) par date de création de notice sous HAL laisse apparaître un cycle d'une période de quatre ans où après un plateau puis maxima local apparaît une chute vers une minimum local qui laisse tout de même sous entrendre effet lié aux période quadriennales des évaluation HCERES. Tandis que la courbe (verte) par date de publication (et non la date de création de la notice sous HAL) montre plutôt une droite croissante sans effet de cycle quadriennal clairement visible.Nous identifions également un phénomène de décroissance importante pour l'année 2020 probablement lié à l'effet de la COVID-19 comme un facteur ayant vraissemblablement impacté le temps dédié à la recherche et la publication au profit d'une gestion des urgences liées aux difficultées d'enseignements.

# ##  Annexe

# ####  Visualisation

# In[ ]:


get_ipython().run_line_magic('matplotlib', 'ipympl')

dfHalUP = pd.read_csv(fileDataUP)

fig, ax = plt.subplots()

# the size of A4 paper
fig.set_size_inches(11.7, 8.27)
sns.set_style("whitegrid")
ax.set(xticks=range(2003,2021))
ax.set_title('HAL UP over years')
sns.lineplot(data=dfHalUP, x="year", y="nbByProducedDate", color="green", label="produced", ax=ax)
sns.lineplot(data=dfHalUP, x="year", y="nbByReleasedDate", color="blue", label="released", ax=ax)
sns.scatterplot(data=dfHalUP, x="year", y="nbByProducedDate", color="green", ax=ax)
p = sns.scatterplot(data=dfHalUP, x="year", y="nbByReleasedDate", color="blue", ax=ax)


# In[ ]:


#p.figure.savefig("./../data/graphic/UP-years_colors.png",dpi=200)


# #### Code d'intégration d'un script  python externe au notebook

# In[ ]:


# The sys module provides functions and variables used to manipulate different parts of the Python runtime environment
# The OS module in Python provides a way of using operating system dependent functionality. The functions that the OS module provides allows you to interface with the underlying operating system that Python is running on – be that Windows, Mac or Linux

import sys
import os

sys.path.insert(0, os.path.abspath('./../code/'))
import computeData as cptd

listLabs = ["UNIV-POITIERS"]
listYears = range(2003,2021)
fileDataUP = https://api.archives-ouvertes.fr/docs/search/?schema=field-types#field-types"
cptd.listCollectionToCSV(listLabs, listYears, fileDataUP)


# ## Print dependencies
# ### Dependences are fundamental to record the computation environnement
# Use watermark to print version of python, ipython, packages, and characteristics of the computer

# In[3]:


# pip install watermark


# In[14]:


# wget is a package to dowload files
#!pip install wget


# In[15]:


get_ipython().run_line_magic('load_ext', 'watermark')

# Python, ipython, packages, and amchine characteristics

get_ipython().run_line_magic('watermark', '-v -m -p wget,pandas,numpy,watermark,seaborn,requests,ipywidgets,openpyxl,matplotlib,sys,os')

# data

print(" ")
get_ipython().run_line_magic('watermark', '-u -n -t -z')

