# Executable paper

## Projet description :

With the aim of training doctoral students and researchers, this project aims to show, from a concrete example, how to develop an executable and reproducible paper.

## Summary :

>What is an executable and reproducible paper ?

>It is a scientific article that skilfully combines.

- text ;
- raw data ;
- code ;
- visualization.

>Why do we need it?

>To allow other researchers to reproduce it or interact with its content.

>How to make and share them?

>We build it using a notebook to write the text component (markdown cell) and the code component (in python or in R). We share it through the MyBinder platform.

# Button to launch the interactive demonstration article with the myBinder remote service


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fgveracruz%2Fexecutable_paper.git/master?urlpath=lab)


